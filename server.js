var path =require("path");
// var { absolutePath } = require("swagger-ui-dist");
var express = require("express");
var app = express();
 
app.use("/api-docs", express.static(path.join(__dirname, "./public/api-docs")));
// app.use(express.static(absolutePath()));
app.use(express.static('public'));
 
app.listen(3030, () => console.log("listening on port " + 3030));